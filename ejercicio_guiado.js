var numSalads = 1;
var saladPrice = 9;
var numBurger = 2;
var burgerPrice = 11;
var tax = 0.21;

function getTotalProductPrice(numProduts, productPrice) {
    return numProduts * productPrice;
}

function addTax(total, tax) {
    return total * tax + total;
}

var total = getTotalProductPrice(numSalads, saladPrice) + getTotalProductPrice(numBurger, burgerPrice);
var billAmount = addTax(total, tax);
console.log(billAmount);
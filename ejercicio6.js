var jugados = 0;
var ganados = 0;
var perdidos = 0;

while ("Jugar de nuevo") {
    var prompt = require('prompt-sync')();
    var jugada = prompt('Escribe "piedra", "papel" o "tijera", o pulsa ENTER para salir: ');
    
    // GUARDAMOS UN NÚMERO ALEATORIO DE 1 AL 9
    var aleatorio = Math.floor((Math.random() * 9) + 1);
    var elige = "";
    if (aleatorio <= 3) {
        elige = "piedra";
    }
    else if (aleatorio <= 6) {
        elige = "papel";
    }
    else {
        elige = "tijera";
    }

        // De aqui en adelante, rellena las plantillas de CODE con lo adecuado ;)
    if ((jugada == "piedra" ) && (elige == "peidra")) {
        console.log(jugada+"-"+elige+": Empate");
    }
    else if ((jugada == "piedra") && (elige == "papel")) {
        console.log(jugada+"-"+elige+": Gana PC");
        perdidos++;
    }
    else if ((jugada == "piedra") && (elige == "tijera")) {
        console.log(jugada+"-"+elige+": Gana Jugador");
        ganados++;
    }
    else if ((jugada == "papel") && (elige == "papel")) {
        console.log(jugada+"-"+elige+": Empate");
    }
    else if ((jugada == "papel") && (elige == "tijera")) {
        console.log(jugada+"-"+elige+": Gana PC");
        perdidos++;
    }
    else if ((jugada == "papel") && (elige == "piedra")) {
        console.log(jugada+"-"+elige+": Gana Jugador");
        ganados++;
    }
    else if ((jugada == "tijera") && (elige == "tijera")) {
        console.log(jugada+"-"+elige+": Empate");
    }
    else if ((jugada == "tijera") && (elige == "piedra")) {
        console.log(jugada+"-"+elige+": Gana PC");
        perdidos++;
    }
    else if ((jugada == "tijera") && (elige == "papel")) {
        console.log(jugada+"-"+elige+": Gana Jugador");
        ganados++;
    } else {
        return
    }
    jugados++;

};

console.log("Partidas jugadas: "+jugados+"<br>");
console.log("Partidas ganadas: "+ganados+"<br>");
console.log("Partidas perdidas: "+perdidos+"<br>");
function getWheelType(diameter) {
    if (diameter <= 10) {
        console.log('Es una rueda para un juguete pequeño.');
    } else if (diameter > 10 && diameter < 20) {
        console.log('Es una rueda para un juguete mediano.');
    } else if (diameter >= 20) {
        console.log('Es una rueda para un juguete grande.');
    }
}
getWheelType(8);    //pequeño
getWheelType(15);   //mediano
getWheelType(40);   //grande
getWheelType(20);   //grande
getWheelType(10);   //pequeño
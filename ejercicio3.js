var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
            'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
            

var prompt = require('prompt-sync')();  // Preparación del elemento prompt-sync

var numeroDNI = parseInt(prompt('Introduce el número de tu DNI: '), 10);    // Input: número DNI
var letraDNI = prompt('Introduce la letra de tu DNI: ').toUpperCase();      // Input: letra DNI



function checkDNI(numeroDNI, letraDNI, letraResto) {

    // Comprobar si el número introducido por el usuario es correcto
    if (numeroDNI < 0 || numeroDNI > 99999999 || Number.isNaN(numeroDNI)) {
        var message = 'Número de DNI no válido.';
        return message
    } 

    // Calcular el resto
    var resto = numeroDNI % 23;

    // Hallar la letra haciendo una iteración sobre el array de las letras
    for (i = 0; i < letras.length; i++) {
        var letraResto = letras[resto];
    }

    // Comprobar si la letra introducida por el usuario es correcta
    if (letraDNI !== letraResto) {
        console.log('DNI Incorrecto');
        console.log('Letra introducida: ' + letraDNI);
        console.log('Quizás tu letra debería ser: ' + letraResto);
    } else {
        console.log('DNI Correcto');
        console.log('DNI introducido: ' + numeroDNI + '-' + letraDNI);
    }

};

// Invocar la función
console.log(checkDNI(numeroDNI, letraDNI));
var num = 9; // Declarar el número de líneas para nuestra pirámide

// Declarando la función
function generatePyramid(num) {
    for (var i = 1; i <= num; i++) {
        var arr = [];
        for (var j = 1; j <= i; j++) {
            arr.push(j);            
        }

        // Reflejamos en consola los datos del array, cada uno en una línea distinta
        console.log(arr.join(" ") + "\n");
    }
}

// Invocando la función
generatePyramid(num);
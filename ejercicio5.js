var texto = 'marvel mola!';

function separateCharacters(texto) {
    var char = []; // Aquí almacenamos cada carácter individualmente en un array
    
    // Loop para iterar sobre cada carácter de nuestro string
    for (i = 0; i <= texto.length-1; i++) {
        if (texto[i] == ' ') {
            // Condicion vacía para que no añada los espacios en blanco
        } else {
            char.push(texto.charAt(i)); // Si la primera condición no se cumple, nos añade los carácteres restantes
        }
    }
    console.log(char.join('-')); // Por último imprimimos el resultado en la consola utilizando el metodo join
}

separateCharacters(texto); // Invocamos la función